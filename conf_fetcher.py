#!/bin/env python

"""
This script fetches all JSON (*.json) files under .conf/ directory and create 
a list with their names, without the extension.

Usage:

    ./conf_fetcher.py

File names list will be included in a dictionary of this form:

    {
        "response": <list_of_file_names>,
        "success": <True|False>
    }

If an error ocurred and the list of files can't be fetched, this script will 
print an empty list under the response value.
"""

import glob
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
CONF_DIR = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'conf')

from pssapi.utils import rest

def _main():
    error = False
    try:
        file_pattern = os.path.join(CONF_DIR, "*.json")
        result = [os.path.splitext(os.path.basename(f))[0] \
                 for f in glob.glob(file_pattern)]
    except Exception:
        error = True
        result = []
    rest.response(sorted(result), success=(not error))

if __name__ == "__main__":
    _main()
