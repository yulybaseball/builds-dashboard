#!/bin/env python

"""
Script to be run remotely to fetch build file content from inside a 
compressed file.

Usage:

    ./_zip_build_fetcher.py <compressed_file> <build_file>

compressed_file: complete path to the compressed file. It must be passed in 
    wrapping it into double quotes.
build_file: path to the build file from inside the compressed file. It must 
    be passed in wrapping it into double quotes.

This script prints the content of the builds file, or the error in case it 
arises.
"""

import os
import sys
import zipfile

def _main():
    result = ""
    try:
        compressed_file = sys.argv[1]
        build_file = sys.argv[2]
        if not os.path.isfile(compressed_file):
            raise Exception("Compressed file doesn't exist.")
        cf_obj = zipfile.ZipFile(compressed_file, 'r')
        result = cf_obj.read(build_file)
    except IndexError:
        result = "Missing parameter(s) when fetching compressed build."
    except Exception as e:
        result = str(e)
    print(result)

if __name__ == "__main__":
    _main()
