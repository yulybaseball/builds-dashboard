#!/bin/env python

"""
This application fetches build files from URLs defined in ./conf/*.json  
and creates a resulting JSON file to be sent to a frontend.

Usage:

    ./main.py <env> [-d datacenter] [-c component] [--byserver]

env -- Environment to fetch the builds from. It matches the name of one of the 
    JSON files defined under ./conf/, without the .json extension, 
    case sensitive.
datacenter -- Datacenter to fetch the builds from.
component -- If this is specified, this script will fetch the builds only for 
    this component.

This script reads the configuration defined in <env>.json file for the passed 
environment, which format should be this:

{
    <group name>: [
        {
            "name": <frontend/backend/service name>,
            "build_url": <url to fetch build in the cloud>,
            "servers": [
                {
                    "name": <server name>,
                    "datacenter": <datacenter this server belongs to>,
                    "build_url": <url to fetch build from specific server>
                },
                {...}
            ]
        },
        {...}
    ],
    <another group name>: [
        ...
    ]
}

Output (JSON format):

[
    {
        "group": <group name (Legacy, Angular, etc...)>,
        "apps": [
            {
                "name": <name of the frontend/backend/service>,
                "cloud": {
                    "build": <build number>,
                    "branch": <branch name>,
                    "commit": <commit>,
                    "date": <date>
                },
                "byserver": [
                    {
                        "name": <server_name>,
                        "build": <build number>,
                        "branch": <branch name>,
                        "commit": <commit>,
                        "date": <date>
                    },
                    {...}
                ]
            },
            {
                <another frontend/backend/service>
            }
        ]
    },
    {
        <another group>
    }
]
"""

import json
import getopt
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
CONF_DIR = os.path.join(os.path.dirname(os.path.realpath(sys.argv[0])), 'conf')

from builds import _builds_fetcher as fetcher
from pssapi.utils import conf, rest, util

SHORT_OPTIONS = "d:c:"
LONG_OPTIONS = "byserver"

def _check_params():
    """Check parameters and options passed in to this script. Return a 
    dictionary with them. Throw an exception if no environment was specified 
    or if more than one was specified.
    """
    if not len(sys.argv) > 1:
        raise Exception("Environment should be passed in as argument.")
    arguments = sys.argv[1:]
    options, args = getopt.gnu_getopt(arguments, shortopts=SHORT_OPTIONS, 
                                      longopts=LONG_OPTIONS)
    if len(args) > 1:
        raise Exception("Only one argument is supported: environment.")
    paramsdict = {'env': args[0]}
    for option in options:
        paramsdict[option[0]] = option[1]
    return paramsdict

def _main():
    result = {}
    try:
        paramsdict = _check_params()
        environment = paramsdict['env']
        conffile = os.path.join(CONF_DIR, '{0}.json'.format(environment))
        if not util.is_json(conffile):
            raise Exception("Conf file is not a valid JSON file.")
        envconf = conf.get_conf(conffile)
        result = fetcher._fetch_builds(envconf, paramsdict)
    except KeyError as ke:
        result['error'] = ("{0} is a required key in the conf file. Please " + 
                           "check documentation.").format(str(ke))
    except Exception as e:
        import traceback
        traceback.print_exc()
        result['error'] = str(e)
    finally:
        rest.response(result, success=('error' not in result))

if __name__ == "__main__":
    _main()
