"""
Needed functions to fetch builds and process the resulting JSON.
"""

import json
from multiprocessing.pool import ThreadPool as Pool
import os
import sys
import urllib.request

from pssapi.utils import conn, threadsafe, util
from pssapi.cmdexe import cmd

COMPRESSED_BUILD_FETCHER_SCRIPT = os.path.join(os.path.dirname(
                    os.path.realpath(sys.argv[0])), '_zip_build_fetcher.py')
BUILD_KEYS = ['build', 'branch', 'commit', 'date']
BUILD_MAIN_KEY = 'build'
TIMEOUT = 10

def _create_build_url(servername, protocol, port, build_file):
    porttext = ":{0}".format(port) if port is not None else ""
    return "{0}://{1}{2}{3}".format(protocol, servername, porttext, build_file)

def _get_build_by_lines(build_txt, lines):
    build = {}
    _build_txt = build_txt
    error = None
    if lines:
        _build_in_lines = build_txt.replace('\r\n','\n').split('\n')
        try:
            _build_txt = " ".join([_build_in_lines[int(line) - 1] \
                                  for line in lines]). replace('\n', ' ')
        except IndexError:
            error = "Fetched build has no the configured line number."
        except Exception as e:
            error = str(e)
    if error:
        build['error'] = error
    else:
        for build_key in BUILD_KEYS:
            build[build_key] = _build_txt \
                    if build_key == BUILD_MAIN_KEY else ""
    return build

def _fill_json_build_values(build_as_json, name=None):
    build = {}
    if name:
        build['name'] = name
    for build_key in BUILD_KEYS:
        try:
            build[build_key] = build_as_json[build_key]
        except KeyError as ke:
            missing_key = str(ke).replace("'", '').replace('"', '')
            build[missing_key] = "Key '{0}' is missing in build file.".\
                                format(missing_key)
    return build

def _get_build_as_json(build_content, build_lines=None):
    try:
        build_as_json = json.loads(build_content)
        isjson = util.is_json(json.dumps(build_as_json), file=False)
    except:
        isjson = False
    return _fill_json_build_values(build_as_json) if isjson else \
           _get_build_by_lines(build_content, build_lines)

def _get_url_page_content(build_url):
    """Connect to build_url and return the content.
    This function is trying to connect twice to the same URL since we noticed
    sometimes it gives timeout during the first attempt.
    """
    def __page_content(b_url):
        with urllib.request.urlopen(b_url, timeout=TIMEOUT) as response:
            return response.read()
    try:
        return __page_content(build_url)
    except urllib.error.HTTPError:
        return __page_content(build_url)

def _fetch_build_url(build_url, build_lines=None):
    """Fetch build from URL parameter build_url.
    Return a dictionary with the keys defined in BUILD_KEYS, which should be 
    the same keys contained in the fetched JSON file.
    """
    build = {}
    try:
        url_content = _get_url_page_content(build_url)
        build = _get_build_as_json(util.bytes2str(url_content), build_lines)
    except Exception as e:
        build['error'] = str(e)
    return build

def _fetch_build_ssh(server_name, user_name, build_file, compressed_path, 
                     build_lines=None):
    build = {}
    try:
        if compressed_path:
            spaces_build_file = "\\\"{0}\\\"".format(build_file)
            spaces_compressed_path = "\\\"{0}\\\"".format(compressed_path)
            stdout, stderr = cmd.pythonexec(user_name, server_name, 
                             COMPRESSED_BUILD_FETCHER_SCRIPT, True, 
                             spaces_build_file, spaces_compressed_path)
        else:
            command = "ssh {0}@{1} cat {2}".format(user_name, server_name, 
                                                   build_file)
            stdout, stderr = cmd.cmdexec(command)
        error = util.bytes2str(stderr).strip()
        if error:
            raise Exception(error)
        output = util.bytes2str(stdout).strip()
        build = _get_build_as_json(output, build_lines)
    except Exception as e:
        build['error'] = str(e)
    return build

def _get_app_server_build(serverconf):
    protocol = serverconf['protocol'] if 'protocol' in serverconf else 'http'
    build_file = serverconf['build_file']
    servername = serverconf['name']
    build_lines = serverconf['lines'] if 'lines' in serverconf else None
    if protocol == 'ssh':
        username = serverconf['user']
        compressed_path = serverconf['compressed_path'] \
                          if 'compressed_path' in serverconf else None
        return _fetch_build_ssh(servername, username, build_file, 
                                compressed_path, build_lines)
    if protocol in ['http', 'https']:
        port = serverconf['port'] if 'port' in serverconf else None
        build_url = _create_build_url(servername, protocol, port, build_file)
        return _fetch_build_url(build_url, build_lines)
    return {'error': "Protocol '{0}' is not allowed.".format(protocol)}

def _server_conf_from_primary_data_center(servers_conf):
    current_data_center = util.bytes2str(conn.current_datacenter().lower())
    for server_conf in servers_conf:
        if server_conf['datacenter'].lower() == current_data_center.lower():
            return server_conf
    return servers_conf[0]

def _get_app_cloud_build(appconf):
    if 'build_file' in appconf:
        return _fetch_build_url(appconf['build_file'], appconf['lines'] \
                                if 'lines' in appconf else None)
    if 'servers' in appconf and len(appconf['servers']) >= 1:
        serverconf = _server_conf_from_primary_data_center(appconf['servers'])
        return _get_app_server_build(serverconf)
    return {'error': "No build file info was provided in the conf file."}

def _builds_by_server(serversconf, options):
    servers_builds = []
    for serverconf in serversconf:
        serv_dc = serverconf['datacenter']
        if ('-d' in options and options['-d'] == serv_dc) \
            or '-d' not in options:
            server_build = _get_app_server_build(serverconf)
            if not 'error' in server_build:
                server_result_build = _fill_json_build_values(server_build, 
                                                name=serverconf['name'])
            else:
                server_result_build = {
                    "name": serverconf['name'],
                    "error": server_build['error']
                }
            servers_builds.append(server_result_build)
    return servers_builds

def _get_app_build(appconf, options):
    """Get build for app defined in appconf, and all its servers, in case 
    --byserver has been specified in the options.
    
    Keyword arguments:
    appconf -- dictionary containing the definition for an application to get 
        the build for. It has this form:
        {
            "name": <app_name>,
            "build_url": <url2fetch_build_in_the_cloud>,
            "servers": [
                {
                    "name": <server name>,
                    "datacenter": <datacenter this server belongs to>,
                    "build_url": <url to fetch build from specific server>
                },
                {...}
            ]
        }
    options -- dictionary specifying the options to apply when fetching builds.
        Keys:
        --byserver: fetch builds from app servers.
        -c: fetch build only if app is the value provided.
        -d: fetch build only if --byserver was provided and the server belongs 
            to the datacenter specified as value.
    
    Return a dictionary with the app build values if there was no error.
        {
            "name": appname,
            "cloud": {
                "build": <build number>,
                "branch": <branch name>,
                "commit": <commit>,
                "date": <date>
            },
            "byserver": [
                {
                    "name": <server_name>,
                    "build": <build number>,
                    "branch": <branch name>,
                    "commit": <commit>,
                    "date": <date>
                },
                {...}
            ]
        }
    If there was an error fetching the build, dictionary has this form:
        {
            "name": appname,
            "cloud": {},
            "byserver": [],
            "error": <error_message>
        }
    """
    appname = appconf['name'].lower()
    if ('-c' in options and options['-c'].lower() == appname) \
       or '-c' not in options:
        result = {
            "name": appname,
            "cloud": {},
            "byserver": []
        }
        if '--byserver' in options:
            serversconf = appconf['servers'] if 'servers' in appconf else []
            result['byserver'] = _builds_by_server(serversconf, options)
        else:
            app_cloud_build = _get_app_cloud_build(appconf)
            if not 'error' in app_cloud_build:
                result['cloud'] = _fill_json_build_values(app_cloud_build)
            else:
                result['error'] = app_cloud_build['error']
        return result
    return {}

def _app_worker(appconf, options, dictresult):
    """Get build for a specific application defined in appconf.
    This function is executed by every thread created for each application.
    It assigns the fetched app build to the dictionary (dictrestult) to be 
    used as the one containing all apps builds. This is possible because 
    dictresult is a thread-safe dict.
    """
    appbuild = _get_app_build(appconf, options)
    if appbuild:
        dictresult['apps'][appbuild['name']] = appbuild

def _keep_apps_order(orderedapps, dictresult):
    """Create and return a list of applications with their builds, in the 
    corresponding order defined in orderedapps.

    Keyword arguments:
    orderedapps -- list with the names of the apps in the desired order.
    dictresult -- dictionary with the apps and their builds. It has this form:
        {
            "group": <group_name>,
            "apps": {
                <app_name>: {<app_build_data>},
                <another_app_name>: {<another_app_build_data>},
                ...
            }
        }

    Return a list of dictionaries:
        [{<app_build_data>}, {<another_app_build_data>}]

    <app_build_data> should have at least the key 'name'.
    """
    appbuilds = dictresult['apps']
    result = []
    for app in orderedapps:
        if app.lower() in appbuilds:
            appbuilds[app.lower()]['name'] = app
            result.append(appbuilds[app.lower()])
        elif app in appbuilds:
            result.append(appbuilds[app])
    return result

def _fetch_builds(envconf, options):
    """Fetch all builds from the apps defined in envconf.
    Return a dictionary with the builds.
    """
    result = []
    for groupconf in envconf:
        groupresult = threadsafe.ThreadSafeDict(
            {
                "group": groupconf['name'],
                "apps": {}
            }
        )
        # safe the order of the apps
        appsdict = [appconf['name'] for appconf in groupconf['apps']]
        # amount of threads to create: one per app
        threadpool = Pool(len(groupconf['apps']))
        for appconf in groupconf['apps']:
            threadpool.apply_async(_app_worker, (appconf, options, groupresult))
        threadpool.close()
        threadpool.join()
        if groupresult['apps']:
            # getting the order of the apps back to its original state
            groupresult['apps'] = _keep_apps_order(appsdict, groupresult)
            result.append(groupresult)
    return result
